import 'react-native-gesture-handler';
import * as React from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {CustumHeader, CustumDrawerContent } from './src'
import { HomeScreen, HomeScreenDetail, SettingScreen, SettingScreenDetail} from './src/tab'
import {NotificationsScreen} from './src/drawer'
import {RegisterScreen, LoginScreen, ConnectionScreen} from './src/auth'
import { IMAGE } from './src/constants/Image'

function PretScreen({navigation}) {
  return(
    <SafeAreaView style={{flex: 1}}>
      <CustumHeader title="Pret" isHome={true} navigation={navigation}/>
    <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
      <Text>Pret</Text>
      <TouchableOpacity style={{marginTop: 20}} onPress={() => navigation.navigate('PretDetail')}>
        <Text>Go Pret Detail</Text>
      </TouchableOpacity>
    </View>
    </SafeAreaView>
  );
}
function PretScreenDetail({navigation}) {
  return(
    <SafeAreaView style={{flex: 1}}>
      <CustumHeader title="Pret" navigation={navigation}/>
    <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
      <Text>Pret</Text>
    </View>
    </SafeAreaView>
  );
}

const Tab = createBottomTabNavigator();

const navOptionHandler = () => ({
  headerShown: false
})

const StackHome = createStackNavigator();

function HomeStack() {
  return (
      <StackHome.Navigator initialRouteName="Home">
        <StackHome.Screen name="Home" component={HomeScreen} options={navOptionHandler}/>
        <StackHome.Screen name="HomeDetail" component={HomeScreenDetail} options={navOptionHandler}/>
      </StackHome.Navigator>
  );
}

const StackSetting = createStackNavigator();

function SettingStack() {
  return (
      <StackSetting.Navigator initialRouteName="Setting">
        <StackSetting.Screen name="Setting" component={SettingScreen} options={navOptionHandler}/>
        <StackSetting.Screen name="SettingDetail" component={SettingScreenDetail} options={navOptionHandler}/>
      </StackSetting.Navigator>
  );
}

const StackPret = createStackNavigator();

function PretStack() {
  return (
      <StackPret.Navigator initialRouteName="Pret">
        <StackPret.Screen name="Pret" component={PretScreen} options={navOptionHandler}/>
        <StackPret.Screen name="PretDetail" component={PretScreenDetail} options={navOptionHandler}/>
      </StackPret.Navigator>
  );
}

function TabNavigator() {
  return (
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size}) => {
            let iconName;

            if( route.name === 'Home') {
              iconName = focused
              ? IMAGE.ICON_HOME
              : IMAGE.ICON_HOME_BLACK;
            } else if( route.name === 'Setting') {
              iconName = focused ? 
              IMAGE.ICON_SETTING
              : IMAGE.ICON_SETTING_BLACK;
            }  else if( route.name === 'Pret') {
              iconName = focused ? 
              require('./src/images/pret.png')
              : require('./src/images/prets.png');
            } 
            return <Image source={iconName} style={{height: 25, width: 25}} resizeMode="contain"/>;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'red',
          inactiveTintColor: 'yellow',
        }}
      >
      <Tab.Screen name="Home" component={HomeStack} />
      <Tab.Screen name="Setting" component={SettingStack} />
      <Tab.Screen name="Pret" component={PretStack} />
    </Tab.Navigator>
  )
}

const Drawer = createDrawerNavigator();

function DrawerNavigator({navigation}) {
return(
  <Drawer.Navigator initialRouteName="MenuTab" 
      drawerContent ={ () => <CustumDrawerContent navigation={navigation}/>}>
        <Drawer.Screen name="MenuTab" component={TabNavigator} />
        <Drawer.Screen name="Notifications" component={NotificationsScreen} />

      </Drawer.Navigator>
)
}

const StackApp = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StackApp.Navigator initialRouteName="Login">
        <StackApp.Screen name="HomeApp" component={DrawerNavigator} options={navOptionHandler}/>
        <StackApp.Screen name="Login" component={LoginScreen} options={navOptionHandler}/> 
        <StackApp.Screen name="Register" component={RegisterScreen} options={navOptionHandler}/>
        <StackApp.Screen name="Connection" component={ConnectionScreen} options={navOptionHandler}/>

      </StackApp.Navigator>
    </NavigationContainer>
  );
}
