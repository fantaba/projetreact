import React, { Component } from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity, StyleSheet} from 'react-native';

export class LoginScreen extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
            <Image style={{height: 30, width: 30}} 
                source={require('../images/logoFewnu1.png')}
                style={{height: 200, width: 220 }}
                />
            <TouchableOpacity style={styles.button} 
            onPress={() => this.props.navigation.navigate('Connection')}>
                <Text style={styles.btntext}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}  
            onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={styles.btntext}>Register</Text>
            </TouchableOpacity>
            </View>
            </SafeAreaView>
  );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
      },
    button: {
        alignSelf: 'center',
        alignItems: 'center',
        padding : 10,
        backgroundColor: '#24A5BC',
        margin: 10,
        height: 50,
        width: 200,
        borderRadius: 50,
    },
    btntext: {
      fontSize: 20,
      color: 'white',
      
    },
    text:{
      color:'blue'
    }
  });