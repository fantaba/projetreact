import React, { Component } from 'react';
import {Text, View , SafeAreaView, TouchableOpacity, StyleSheet,TextInput} from 'react-native';
import { CustumHeader } from '../index';

export class RegisterScreen extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
            <CustumHeader title="Register" navigation={this.props.navigation}/>
            <View style={styles.container}>
            <Text style={styles.text}>Créer un compte</Text>
            <Text> </Text>

      <Text> </Text>
      <TextInput style={styles.input}
       placeholder= 'Nom'
      />

      <TextInput style={styles.input}
       placeholder= 'Prenom'
      />
      
      <TextInput style={styles.input}
       keyboardType='numeric'
       placeholder= 'Telephone'
      />
      
      <TextInput style={styles.input}
       placeholder = 'E-mail'
      />
      
      <TextInput style={styles.input}
       placeholder= 'Mot de passe'
       secureTextEntry={ true }
      />
      <TextInput style={styles.input}
       placeholder= 'Confirme Mot de passe'
       secureTextEntry={ true }
      />
      <Text> </Text>

      <Text> </Text>
      <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('HomeApp')}>      
          <Text style={styles.btntext}>S'inscrire</Text>
      </TouchableOpacity>
            </View>
            </SafeAreaView>
  );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#24A5BC',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 500,
    paddingRight: 500,
    
  },
  input : {
    borderBottomWidth : 1, 
    borderColor : 'white',
    padding : 8,
    margin : 10,
    width : 275,
  },
  button: {
      alignSelf: 'center',
      alignItems: 'center',
      padding : 10,
      backgroundColor: 'white',
      margin: 10,
      height: 50,
      width: 200,
      borderRadius: 50,
  },
  btntext: {
    fontSize: 20,
    color: '#24A5BC',
    
  },
  text:{
    fontStyle: 'italic',
    fontSize: 25,
    color:'white'
  }
});

