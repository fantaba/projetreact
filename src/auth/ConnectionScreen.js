import React, { Component } from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity, Button, ScrollView, StyleSheet,TextInput} from 'react-native';
import { CustumHeader } from '../index';

export class ConnectionScreen extends Component {
  render(){
      return (
    <SafeAreaView style={{flex: 1}}>
    <CustumHeader title="Register" navigation={this.props.navigation}/>
    <View style={styles.container}>
      <Text style={styles.text}>Se connecter</Text>
      <Text> </Text>

      <Text> </Text>
      <TextInput style={styles.input}
       placeholder= 'E-mail ou Téléphone'
       onChangeText={(val) => setEmail(val)}
      />
      
      <TextInput style={styles.input}
       placeholder= 'Mot de Passe'
       //onChangeText={(val) => setPassword(val)}
       secureTextEntry={ true }
      />
      <Text> </Text>
      <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('HomeApp')}>      
          <Text style={styles.btntext}>Se connecter</Text>
      </TouchableOpacity>
      <Text> </Text>
      <Text>Ou</Text>
      <Text> </Text>
      <TouchableOpacity style={styles.buttonA} onPress={() => this.props.navigation.navigate('Login')}>      
          <Text style={styles.btntextA}>Login avec Facebook </Text>
      </TouchableOpacity>
    </View>
    </SafeAreaView>
  );
}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#24A5BC',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 500,
    paddingRight: 500,
    
  },
  button: {
    alignSelf: 'center',
    alignItems: 'center',
    padding : 10,
    backgroundColor: 'white',
    margin: 10,
    height: 50,
    width: 250,
    borderRadius: 50,
},
  buttonA: {
    backgroundColor: '#4267b2',
    alignSelf: 'stretch',
      alignItems: 'center',
      padding : 10,
      margin: 10,
      height: 50,
      borderRadius: 50,
  },
  btntextA: {
    fontSize: 20,
    color: 'white',
  },
  
  btntext: {
    fontSize: 20,
    color: '#24A5BC',
    
  },
  input : {
    borderBottomWidth : 1, 
    borderColor : 'white',
    padding : 8,
    margin : 10,
    width : 275,
    borderRadius: 50,
  },
  text:{
    fontStyle: 'italic',
    fontSize: 25,
    color:'white'
  }
});
