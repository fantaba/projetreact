import React, { Component } from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity, Button, ScrollView} from 'react-native';
import { CustumHeader } from '../index';
import { RVText } from '../core';

export class HomeScreen extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
            <CustumHeader title="Home" isHome={true} navigation={this.props.navigation}/>
          <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
            <RVText style={{fontSize: 30}} content="VENTES"/>
            <TouchableOpacity style={{marginTop: 20}} onPress={() => this.props.navigation.navigate('HomeDetail')}>
              <RVText content="Go Home Detail"/>
            </TouchableOpacity>
          </View>
          </SafeAreaView>
  );
    }
}