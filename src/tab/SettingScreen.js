import React, { Component } from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity, Button, ScrollView} from 'react-native';
import { CustumHeader } from '../index'

export class SettingScreen extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
        <CustumHeader title="Setting" isHome={true} navigation={this.props.navigation}/>
        <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
        <Text>Depenses</Text>
        <TouchableOpacity style={{marginTop: 20}} onPress={() => this.props.navigation.navigate('SettingDetail')}>
            <Text>Go Setting Detail</Text>
        </TouchableOpacity>
        </View>
        </SafeAreaView>
  );
    }
}