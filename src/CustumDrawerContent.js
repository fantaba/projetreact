import React, { Component } from 'react';
import {Text, View , SafeAreaView, Image, TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import { IMAGE } from './constants/Image'

export class CustumDrawerContent extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1, backgroundColor: '#088498'}}>
              <View style={{height: 150, alignItems: 'center', justifyContent: 'center'}}>
              <Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_PROFILE1}
                style={{height: 120, width: 120 }}
                />
              </View>
            <ScrollView style={{marginLeft: 5}}>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}>
                <Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Accueil</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('Notifications')}>
                <Text style={styles.text}>
                  <Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Journalier</Text>
              </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}> 
                <Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Hebdomadaire</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}><Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Mensuel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}><Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Annuel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}><Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> Contactez-nous</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('MenuTab')}>
                <Text style={styles.text}><Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_HOME1}
                style={{height: 15, width: 20 }}
                /> A propos</Text>
            </TouchableOpacity>
            </ScrollView>
            <TouchableOpacity style={{marginTop: 20}} 
              onPress={() => this.props.navigation.navigate('Login')}>
                <Text style={styles.text}><Image style={{height: 30, width: 30, marginLeft: 5}} 
                source={IMAGE.ICON_PROFILE2}
                style={{height: 15, width: 20 }}
                /> Déconnection</Text>
            </TouchableOpacity>
            
            </SafeAreaView>
          );
    }
}
const styles = StyleSheet.create({
  text:{
    color:'white'
  }
});