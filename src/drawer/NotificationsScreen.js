import React, { Component } from 'react';
import {Text, View , SafeAreaView} from 'react-native';
import { CustumHeader } from '../index'

export class NotificationsScreen extends Component {
    render(){
        return(
            <SafeAreaView style={{flex: 1}}>
            <CustumHeader title="Notifications" navigation={this.props.navigation}/>
            <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
            <Text>Notifications Screen</Text>
            </View>
            </SafeAreaView>
  );
    }
}