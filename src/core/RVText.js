
import {Text} from 'react-native';
import React, { Component } from 'react';

export class RVText extends Component {
    constructor(props){
        super(props)
    }
    render(){
        const {content, style} = this.props
        return(
        <Text {...this.props} style = {[{fontSize: 18}, style]}>{content}</Text>
        )
    }
}
