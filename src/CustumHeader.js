import React, { Component } from 'react';
import {Text, View , Image, TouchableOpacity} from 'react-native';
import { IMAGE } from './constants/Image'

export class CustumHeader extends Component {
    render(){
      let {navigation, isHome, title} = this.props
        return(
            <View style={{flexDirection: 'row', height: 50}}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          {
            isHome?
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image style={{height: 30, width: 30, marginLeft: 5}} 
        source={IMAGE.ICON_MENU}
        resizeMode="contain"/>
            </TouchableOpacity>
        : 
        <TouchableOpacity style={{flexDirection: 'row', alignItems:'center'}}
        onPress={() => navigation.goBack()}>
          <Image style={{height: 25, width: 25, marginLeft: 5}} 
        source={IMAGE.ICON_BACK}
        resizeMode="contain"
        />
        <Text>Back</Text>
        </TouchableOpacity>
          }
        </View>
      
    <View style={{flex: 1.5, justifyContent: 'center'}}><Text style={{textAlign: 'center', color: '#38B8CF', fontStyle: 'italic'}}><h1>Fewnu</h1></Text></View>
      <View style={{flex: 1}}></View>
    </View>
  );
    }
}