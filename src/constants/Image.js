const IMAGE = {
    ICON_PROFILE: require('../images/logoFewnu1.png'),
    ICON_PROFILE1: require('../images/profile1.png'),
    ICON_PROFILE2: require('../images/shutdown_26px.png'), 
    ICON_PROFILE3: require('../images/facebook.png'),
    ICON_MENU: require('../images/menu.png'),
    ICON_BACK: require('../images/back.png'), 

    ICON_HOME: require('../images/sale.png'), 
    ICON_HOME1: require('../images/home_page_24px.png'),
    ICON_HOME_BLACK: require('../images/vente.png'),
    ICON_SETTING: require('../images/depense.png'),
    ICON_SETTING_BLACK: require('../images/depenses.png')
}

export {IMAGE}